package com.locationnotes.gyroproject.usecase.usecase

import android.location.Location
import com.locationnotes.gyroproject.usecase.location.UserLocationTracker
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class NotifyDisplacementExceedUseCaseTest {

    @Test
    fun `return true when displacement exceeds 10 meters`(): Unit = runBlocking {
        val tracker = mock(UserLocationTracker::class.java)
        val lastKnowLastLocationUseCase = mock(GetLastKnowLocationUseCase::class.java)

        `when`(tracker.getUserMovementFlow()).thenReturn(flow {
            emit(Result.success(
                Location("Point A").apply {
                    latitude = 14.594606314161275
                    longitude = 121.06075463210558
                }
            ))
        })

        `when`(lastKnowLastLocationUseCase.invoke()).thenReturn(flow {
            emit(
                Location("Point B").apply {
                    latitude = 14.594625077346143
                    longitude = 121.06175125380763
                }
            )
        })

        val mockedUseCase = NotifyDisplacementExceedUseCase(tracker, lastKnowLastLocationUseCase)

        // 10 meters
        val currentData = mockedUseCase(10F).first().first
        assertThat(currentData).isTrue()
    }

    @Test
    fun `return false when displacement is below 10 meters`(): Unit = runBlocking {
        val tracker = mock(UserLocationTracker::class.java)
        val lastKnowLastLocationUseCase = mock(GetLastKnowLocationUseCase::class.java)

        `when`(tracker.getUserMovementFlow()).thenReturn(flow {
            emit(Result.success(
                Location("Point A").apply {
                    latitude = 14.594606314161275
                    longitude = 121.06075463210558
                }
            ))
        })

        `when`(lastKnowLastLocationUseCase.invoke()).thenReturn(flow {
            emit(
                Location("Point B").apply {
                    latitude = 14.594606314161276
                    longitude = 121.06075463210558
                }
            )
        })

        val mockedUseCase = NotifyDisplacementExceedUseCase(tracker, lastKnowLastLocationUseCase)

        // 10 meters
        val currentData = mockedUseCase(10F).first().first
        assertThat(currentData).isFalse()
    }

}