package com.locationnotes.gyroproject.data.repository

import androidx.core.net.toUri
import com.locationnotes.gyroproject.data.network.NetworkVideoDataSource
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner
import java.io.IOException

@RunWith(RobolectricTestRunner::class)
class VideoRepositoryImplTest {

    @Test
    fun `check if Result catches the exception from fake network`(): Unit = runBlocking {
        val dataSource = mock(NetworkVideoDataSource::class.java)
        `when`(dataSource.getLatestVideo()).thenAnswer {
            throw IOException("")
        }

        val rep = VideoRepositoryImpl(dataSource)

        val response = rep.featuredVideoFlow().first()
        assertThat(response.isFailure).isTrue()
    }

    @Test
    fun `check if the Result status is successful when the api returns a correct URI`(): Unit =
        runBlocking {
            val dataSource = mock(NetworkVideoDataSource::class.java)
            `when`(dataSource.getLatestVideo()).thenReturn("http://example.com".toUri())

            val rep = VideoRepositoryImpl(dataSource)

            val response = rep.featuredVideoFlow().first()
            assertThat(response.isSuccess).isTrue()
        }

}