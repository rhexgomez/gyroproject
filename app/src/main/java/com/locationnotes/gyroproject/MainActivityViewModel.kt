package com.locationnotes.gyroproject

import android.location.Location
import android.net.Uri
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.locationnotes.gyroproject.usecase.usecase.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    private val getLastKnowLocation: GetLastKnowLocationUseCase,
    private val getUserMovement: GetUserMovementLocationUseCase,
    private val notifyRadiusUseCase: NotifyDisplacementExceedUseCase,
    private val saveLastLocationUseCase: SaveLastLocationUseCase,
    private val getFeaturedVideo: GetFeaturedVideoUseCase
) : ViewModel() {
    companion object {
        private const val METER = 10F
    }

    private val _userDisplacementIs10Meters = notifyRadiusUseCase(METER)
        .shareIn(scope = viewModelScope, started = SharingStarted.Lazily)
        .onEach {
            if (it.first) {
                delayStoreLastLocation(it.second)
            }
        }

    val userDisplacementIs10Meters: Flow<Boolean>
        get() = _userDisplacementIs10Meters.map {
            it.first
        }

    private val _video = getFeaturedVideo()
        .filter { it.isSuccess }
        .map { it.getOrNull() }
        .filterNotNull()
        .shareIn(scope = viewModelScope, started = SharingStarted.Lazily)

    val videoFlow: Flow<Uri> get() = _video

    val getMovement: Flow<Result<Location>> = getUserMovement()
        .shareIn(scope = viewModelScope, started = SharingStarted.Lazily)

    init {
        viewModelScope.launch {
            val isEmpty = getLastKnowLocation().firstOrNull() == null
            if (isEmpty) {
                val firstData = getUserMovement().filter { it.isSuccess }.first().getOrThrow()
                saveLastLocationUseCase(firstData)
            }
        }
    }

    private fun delayStoreLastLocation(lastLocation : Location) = viewModelScope.launch {
        // We are delaying so that the UI will a chance to display the "true" status
        delay(1500)
        saveLastLocationUseCase.invoke(lastLocation)
    }
}