package com.locationnotes.gyroproject.util

class FunctionThrottleHelper(private val throttleTime: Long = 100L) {

    private var lastUpdate: Long = 0L

    // This function will be blocked from execution if called
    // below the configured throttleTime.
    fun execute(block: () -> Unit) {
        val zUpdateTimestamp = System.currentTimeMillis()
        if ((zUpdateTimestamp - lastUpdate) > throttleTime) {
            lastUpdate = zUpdateTimestamp
            block()
        }
    }
}