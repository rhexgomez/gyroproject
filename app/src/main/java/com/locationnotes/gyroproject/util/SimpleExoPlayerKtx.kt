package com.locationnotes.gyroproject.util

import com.google.android.exoplayer2.SimpleExoPlayer

private const val COMMON_MOVEMENT = .20

fun SimpleExoPlayer.seekPreviousByPercentage(m: Double = COMMON_MOVEMENT) {
    val movement = duration.toDouble() * m
    seekTo((currentPosition - movement.toLong()))
}

fun SimpleExoPlayer.seekForwardByPercentage(m: Double = COMMON_MOVEMENT) {
    val movement = duration.toDouble() * m
    seekTo((currentPosition + movement.toLong()))
}