package com.locationnotes.gyroproject

import android.Manifest
import android.content.Context
import android.media.AudioManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.locationnotes.gyroproject.databinding.ActivityMainBinding
import com.locationnotes.gyroproject.usecase.gyroscope.Gyroscope
import com.locationnotes.gyroproject.usecase.gyroscope.GyroscopeListener
import com.locationnotes.gyroproject.usecase.gyroscope.GyroscopeState
import com.locationnotes.gyroproject.util.launchOnStartedSafely
import com.locationnotes.gyroproject.util.seekForwardByPercentage
import com.locationnotes.gyroproject.util.seekPreviousByPercentage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), GyroscopeListener {

    companion object {
        private const val VID_DELAY = 4000L // 4 seconds
    }

    private lateinit var player: SimpleExoPlayer
    private lateinit var gyroscope: Gyroscope

    private lateinit var audioManager: AudioManager

    private val viewModel by viewModels<MainActivityViewModel>()

    private var forwardMovement = 0.10
    private var prevMovement = 0.10

    private val locationPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions: Map<String, Boolean> ->
        when {
            permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                // Precise location access granted.
            }
            permissions.getOrDefault(Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {
                //
            }
            else -> {
                Toast.makeText(this, "Goodbye bruh! Don't use me again!", Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        locationPermissionRequest.launch(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )
        audioManager =
            this.applicationContext.getSystemService(Context.AUDIO_SERVICE) as AudioManager

        player = SimpleExoPlayer.Builder(this.applicationContext).build().also {
            binding.player.player = it
            it.prepare()
        }

        gyroscope = Gyroscope(this.applicationContext)

        launchOnStartedSafely {
            viewModel.getMovement
                .filter { it.isFailure }
                .collect {
                    Toast.makeText(
                        this@MainActivity,
                        "Enable Location Manager from your status bar option!",
                        Toast.LENGTH_LONG
                    ).show()
                    delay(1000)
                    finish()
                }
        }

        launchOnStartedSafely {
            viewModel.userDisplacementIs10Meters.collect {

                binding.textView.text =
                    if (it) getString(R.string.true_title) else getString(R.string.false_title)

                if (it) {
                    player.apply {
                        seekTo(0)
                        playWhenReady = true
                    }
                }
            }
        }

        launchOnStartedSafely {
            viewModel.videoFlow.collect {
                delay(VID_DELAY)
                player.setMediaItem(MediaItem.fromUri(it))
            }
        }
    }

    override fun onStart() {
        super.onStart()
        gyroscope.onStart(this)
        player.play()
    }

    override fun onPause() {
        super.onPause()
        player.pause()
    }

    override fun onStop() {
        super.onStop()
        gyroscope.onStop()
        player.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        player.apply {
            stop()
            release()
        }
    }

    override fun onUpdateListener(state: GyroscopeState) {
        when (state) {
            GyroscopeState.SHAKING -> {
                player.pause()
            }
            GyroscopeState.PLAY_PREVIOUS -> {
                player.seekPreviousByPercentage(prevMovement)
                prevMovement += 0.5
                forwardMovement = 0.10
            }
            GyroscopeState.PLAY_FORWARD -> {
                player.seekForwardByPercentage(forwardMovement)
                forwardMovement += 0.5
                prevMovement = 0.10
            }
            GyroscopeState.VOLUME_UP -> {
                audioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND);
            }
            GyroscopeState.VOLUME_DOWN -> {
                audioManager.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND);
            }
        }
    }
}