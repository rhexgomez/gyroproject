package com.locationnotes.gyroproject.data.hilt

import com.locationnotes.gyroproject.data.location.FusedLocationDataSource
import com.locationnotes.gyroproject.data.location.LocationDataSource
import com.locationnotes.gyroproject.data.location.UserLocationTrackerImpl
import com.locationnotes.gyroproject.data.network.NetworkVideoDataSource
import com.locationnotes.gyroproject.data.network.NetworkVideoDataSourceImpl
import com.locationnotes.gyroproject.data.repository.VideoRepositoryImpl
import com.locationnotes.gyroproject.usecase.location.UserLocationTracker
import com.locationnotes.gyroproject.usecase.repository.VideoRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface DataModule {

    @Singleton
    @Binds
    fun bindsLocationSource(locationService: FusedLocationDataSource): LocationDataSource

    @Singleton
    @Binds
    fun bindsLocation(location: UserLocationTrackerImpl): UserLocationTracker

    @Singleton
    @Binds
    fun bindsVidRepo(repo: VideoRepositoryImpl): VideoRepository

    @Singleton
    @Binds
    fun bindsNetworkVidRepo(repo: NetworkVideoDataSourceImpl): NetworkVideoDataSource
}