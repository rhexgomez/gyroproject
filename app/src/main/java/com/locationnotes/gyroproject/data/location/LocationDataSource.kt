package com.locationnotes.gyroproject.data.location

import android.location.Location
import kotlinx.coroutines.flow.Flow

interface LocationDataSource {

    fun getInitialUserLocationFlow(): Flow<Result<Location>>

    fun getUserMovementFlow(): Flow<Result<Location>>

}