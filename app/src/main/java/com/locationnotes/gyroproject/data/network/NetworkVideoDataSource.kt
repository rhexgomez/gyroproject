package com.locationnotes.gyroproject.data.network

import android.net.Uri

interface NetworkVideoDataSource {

    suspend fun getLatestVideo(): Uri
}