package com.locationnotes.gyroproject.data.location

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Looper
import android.util.Log
import com.google.android.gms.location.*
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class FusedLocationDataSource @Inject constructor(
    @ApplicationContext context: Context,
    applicationCoroutineScope: CoroutineScope
) :
    LocationDataSource {

    companion object {
        private const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
        private const val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2
    }


    @OptIn(ExperimentalCoroutinesApi::class)
    private val location: Flow<Result<Location>> = locationCallbackToFlow()
        .shareIn(scope = applicationCoroutineScope, started = SharingStarted.WhileSubscribed())

    private val firstLocation: Flow<Result<Location>> = flow {
        emit(location.first())
    }

    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    private val settingsClient = LocationServices.getSettingsClient(context)
    private val locationRequest = LocationRequest.create().apply {
        interval = UPDATE_INTERVAL_IN_MILLISECONDS
        fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private val locationSettingRequest = with(LocationSettingsRequest.Builder()) {
        addLocationRequest(locationRequest)
        build()
    }

    override fun getInitialUserLocationFlow(): Flow<Result<Location>> {
        return firstLocation
    }

    override fun getUserMovementFlow(): Flow<Result<Location>> {
        return location
    }

    // https://github.com/Kotlin/kotlinx.coroutines/issues/2368
    // We need to wrap the response here so that exception
    // will not be invoked immediately.
    @SuppressLint("MissingPermission")
    @OptIn(ExperimentalCoroutinesApi::class)
    private fun locationCallbackToFlow() = callbackFlow<Result<Location>> {
        val callback = object : LocationCallback() {
            override fun onLocationResult(lr: LocationResult) {
                trySend(Result.success(lr.lastLocation))
            }
        }

        settingsClient.checkLocationSettings(locationSettingRequest)
            .addOnSuccessListener {
                fusedLocationClient.requestLocationUpdates(
                    locationRequest,
                    callback,
                    Looper.getMainLooper()
                )
            }.addOnFailureListener {
                trySend(Result.failure(it))
            }

        awaitClose {
            fusedLocationClient.removeLocationUpdates(callback)
        }
    }
}