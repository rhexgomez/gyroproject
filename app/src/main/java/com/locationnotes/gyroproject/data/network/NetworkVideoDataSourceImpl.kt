package com.locationnotes.gyroproject.data.network

import android.net.Uri
import androidx.core.net.toUri
import javax.inject.Inject

class NetworkVideoDataSourceImpl @Inject constructor() : NetworkVideoDataSource {
    override suspend fun getLatestVideo(): Uri {
        return "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WeAreGoingOnBullrun.mp4".toUri()
    }
}