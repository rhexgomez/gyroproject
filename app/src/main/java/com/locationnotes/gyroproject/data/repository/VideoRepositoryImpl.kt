package com.locationnotes.gyroproject.data.repository

import android.net.Uri
import com.locationnotes.gyroproject.data.network.NetworkVideoDataSource
import com.locationnotes.gyroproject.usecase.repository.VideoRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class VideoRepositoryImpl @Inject constructor(private val network: NetworkVideoDataSource) :
    VideoRepository {

    override fun featuredVideoFlow(): Flow<Result<Uri>> {
        return flow {
            emit(kotlin.runCatching {
                network.getLatestVideo()
            })
        }
    }
}