package com.locationnotes.gyroproject.data.location

import android.content.Context
import android.location.Location
import com.locationnotes.gyroproject.usecase.location.UserLocationTracker
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

class UserLocationTrackerImpl @Inject constructor(
    @ApplicationContext val context: Context,
    private val locationSource: LocationDataSource
) : UserLocationTracker {

    private val sharedFlow = MutableStateFlow<Location?>(null)

    override fun getLast10MeterLocation(): Flow<Location?> {
        return sharedFlow
    }

    override fun getUserMovementFlow(): Flow<Result<Location>> {
        return locationSource.getUserMovementFlow()
    }

    override suspend fun setLast10MeterLocation(location: Location) {
        sharedFlow.tryEmit(location)
    }
}