package com.locationnotes.gyroproject.usecase.usecase

import android.location.Location
import com.locationnotes.gyroproject.usecase.location.UserLocationTracker
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetLastKnowLocationUseCaseImpl @Inject constructor(private val tracker: UserLocationTracker) :
    GetLastKnowLocationUseCase {

    override operator fun invoke(): Flow<Location?> {
        return tracker.getLast10MeterLocation()
    }
}