package com.locationnotes.gyroproject.usecase.usecase

import android.location.Location
import android.util.Log
import com.locationnotes.gyroproject.usecase.location.UserLocationTracker
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@ViewModelScoped
class SaveLastLocationUseCase @Inject constructor(
    private val tracker: UserLocationTracker
) {

    suspend operator fun invoke(lastLocation: Location) {
        tracker.setLast10MeterLocation(lastLocation)
    }
}