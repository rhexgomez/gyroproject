package com.locationnotes.gyroproject.usecase.location

import android.location.Location
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

interface UserLocationTracker {

    fun getLast10MeterLocation(): Flow<Location?>

    fun getUserMovementFlow(): Flow<Result<Location>>

    suspend fun setLast10MeterLocation(location: Location)
}