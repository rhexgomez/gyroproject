package com.locationnotes.gyroproject.usecase.usecase

import android.location.Location
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import com.locationnotes.gyroproject.usecase.location.UserLocationTracker
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@ViewModelScoped
class NotifyDisplacementExceedUseCase @Inject constructor(
    private val tracker: UserLocationTracker,
    private val getLastKnowLocation: GetLastKnowLocationUseCase
) {

    operator fun invoke(inMeter: Float): Flow<Pair<Boolean, Location>> {
        return tracker.getUserMovementFlow()
            .filter { it.isSuccess }
            .map { it.getOrNull() }
            .filterNotNull()
            .combine(getLastKnowLocation().filterNotNull()) { current: Location, initial: Location ->
                SphericalUtil.computeDistanceBetween(
                    LatLng(
                        current.latitude,
                        current.longitude,
                    ),
                    LatLng(
                        initial.latitude,
                        initial.longitude
                    )
                ) to current
            }
            .map {
                (it.first >= inMeter) to it.second
            }
    }
}