package com.locationnotes.gyroproject.usecase.usecase

import android.location.Location
import android.util.Log
import com.locationnotes.gyroproject.usecase.location.UserLocationTracker
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetUserMovementLocationUseCase @Inject constructor(private val tracker: UserLocationTracker) {

    operator fun invoke(): Flow<Result<Location>> {
        return tracker.getUserMovementFlow()
    }
}