package com.locationnotes.gyroproject.usecase.gyroscope

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import com.locationnotes.gyroproject.util.FunctionThrottleHelper
import com.squareup.seismic.ShakeDetector
import kotlin.math.abs

class Gyroscope(context: Context) : SensorEventListener, ShakeDetector.Listener {

    private var listener: GyroscopeListener? = null
    private val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private val gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
    private val shakeDetector = ShakeDetector(this).apply {
        setSensitivity(ShakeDetector.SENSITIVITY_LIGHT)
    }

    fun onStart(listener: GyroscopeListener) {
        this.listener = listener
        shakeDetector.start(sensorManager)
        sensorManager.registerListener(this, gyroscopeSensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    fun onStop() {
        listener = null
        shakeDetector.stop()
        sensorManager.unregisterListener(this)
    }

    private var lastZ = 0F
    private var lastX = 0F

    private var throttleHelperZ = FunctionThrottleHelper()
    private var throttleHelperX = FunctionThrottleHelper()

    override fun onSensorChanged(event: SensorEvent) {

        val currentX = event.values[X_INDEX]
        val currentZ = event.values[Z_INDEX]

        // it means time to update Z
        val gapZ = currentZ - lastZ
        val gapX = currentX - lastX
        // When gap is positive
        if (gapZ > 0F && gapZ > DISTANCE_MIN_GAP) {
            throttleHelperZ.execute(this::executePlayForward)
        } else if (gapZ < 0F && abs(gapZ) > DISTANCE_MIN_GAP) {
            throttleHelperZ.execute(this::executePlayPrevious)
        }

        if (gapX > 0F && gapX > DISTANCE_MIN_GAP) {
            throttleHelperX.execute(this::executeVolumeUp)
        } else if (gapX < 0F && abs(gapX) > DISTANCE_MIN_GAP) {
            throttleHelperX.execute(this::executeVolumeDown)
        }

        lastZ = currentZ
        lastX = currentX
    }

    private fun executePlayForward() {
        listener?.onUpdateListener(GyroscopeState.PLAY_FORWARD)
    }

    private fun executeVolumeUp() {
        listener?.onUpdateListener(GyroscopeState.VOLUME_UP)
    }

    private fun executePlayPrevious() {
        listener?.onUpdateListener(GyroscopeState.PLAY_PREVIOUS)
    }

    private fun executeVolumeDown(){
        listener?.onUpdateListener(GyroscopeState.VOLUME_DOWN)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        // DO NOTHING
    }

    companion object {
        private const val Z_INDEX = 2
        private const val X_INDEX = 0
        private val DISTANCE_MIN_GAP = 0.05F
    }

    override fun hearShake() {
        listener?.onUpdateListener(GyroscopeState.SHAKING)
    }
}