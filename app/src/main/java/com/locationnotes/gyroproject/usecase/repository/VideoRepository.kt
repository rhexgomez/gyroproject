package com.locationnotes.gyroproject.usecase.repository

import android.net.Uri
import kotlinx.coroutines.flow.Flow

interface VideoRepository {

    fun featuredVideoFlow(): Flow<Result<Uri>>
}