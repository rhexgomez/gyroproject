package com.locationnotes.gyroproject.usecase.gyroscope

enum class GyroscopeState {
    SHAKING, PLAY_PREVIOUS, PLAY_FORWARD, VOLUME_UP, VOLUME_DOWN
}