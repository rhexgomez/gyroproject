package com.locationnotes.gyroproject.usecase.usecase

import android.net.Uri
import com.locationnotes.gyroproject.usecase.repository.VideoRepository
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetFeaturedVideoUseCase @Inject constructor(private val repo: VideoRepository) {

    operator fun invoke(): Flow<Result<Uri>> {
        return repo.featuredVideoFlow()
    }
}