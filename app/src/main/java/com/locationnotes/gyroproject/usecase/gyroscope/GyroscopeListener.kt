package com.locationnotes.gyroproject.usecase.gyroscope

interface GyroscopeListener {

    fun onUpdateListener(state: GyroscopeState)
}