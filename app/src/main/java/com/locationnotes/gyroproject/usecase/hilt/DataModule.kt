package com.locationnotes.gyroproject.usecase.hilt

import com.locationnotes.gyroproject.usecase.usecase.GetLastKnowLocationUseCase
import com.locationnotes.gyroproject.usecase.usecase.GetLastKnowLocationUseCaseImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(ViewModelComponent::class)
interface DataModule {

    @ViewModelScoped
    @Binds
    fun bindsGetLastKnow(usercase: GetLastKnowLocationUseCaseImpl): GetLastKnowLocationUseCase
}